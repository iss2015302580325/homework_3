package Database;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by WeiZehao on 16/11/9.
 * This class is for a test which will output current time.
 */
public class DBTest
{
    public static final String url = "jdbc:mysql://localhost:3306/songfingerDB?" +
            "useUnicode=true&characterEncoding=utf-8&useSSL=false";
    public Connection con;
    public ResultSet resultSet;
    public Statement stmt;


    public DBTest()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url,"root","");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getTime()
    {
        try
        {
            stmt = con.createStatement();
            String sql = "SELECT NOW()";
            resultSet = stmt.executeQuery(sql);
            while (resultSet.next())
            {
                System.out.println(resultSet.getString(1));
            }
            this.con.close();
            this.stmt.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
