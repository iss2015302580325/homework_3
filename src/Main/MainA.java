package Main;

import Database.DBManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by WeiZehao on 16/11/7.
 * This class is to establish a database and insert thirty songs' information into it.
 * This can be accomplished in 20 minutes.
 */

public class MainA
{
    public static void main(String[] args)
    {
        ProjectManager projectManager = new ProjectManager();
        DBManager dbManager = new DBManager();
        File file = new File("doc/musicList.txt");
        File file2 = new File("doc/nameList.txt");
        try
        {
            Scanner scanner = new Scanner(file);
            Scanner scanner2 = new Scanner(file2);
            while(scanner.hasNext())
            {
                try
                {
                    // insert info
                    dbManager.insertData(projectManager.getHashes(scanner.nextLine()),
                            projectManager.getName(scanner2.nextLine()));
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }

            dbManager.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
    }
}
