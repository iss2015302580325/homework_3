package Main;

import MatchSong.MatchSong;

import java.io.IOException;

/**
 * Created by WeiZehao on 16/11/10.
 * This class is for matching songs
 */

public class MainC
{
    public static void main(String[] args)
    {
        // match songs
        long startTime = System.currentTimeMillis();
        MatchSong matchSong = new MatchSong();
        String url = "musicMatch/Derek Duke - Numbani.wav";
        try
        {
            matchSong.getScores(url);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();
        System.out.println("程序运行时间： "+(endTime - startTime) / 1000.0 +"s");
    }
}
