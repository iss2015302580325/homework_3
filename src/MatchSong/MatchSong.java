package MatchSong;

import Database.DBManager;
import Main.ProjectManager;
import homework_3_by_ly.ShazamHash;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


/**
 * Created by ChenCheng on 2016/11/10.
 * This class is to compute every potential song's score and print the top 5.
 */
public class MatchSong
{
    private static final int SongN = 30;//歌曲数
    static ArrayList<PotentialSong> songs = new ArrayList<PotentialSong>();
    private ResultSet resultSet;
    private String[] nameList = new String[SongN];
    private Scanner scanner;

    public void getScores(String path) throws IOException
    {

        ProjectManager p1 = new ProjectManager();
        DBManager dbManager = new DBManager();
        File file = new File("doc/nameList.txt");
        scanner = new Scanner(file);
        int i = 0;
        while (scanner.hasNext())
        {
            nameList[i] = scanner.nextLine();
            i++;
        }

        for(i=0; i<SongN; i++)
        {
            PotentialSong pSong = new PotentialSong();
            pSong.id = i+1;
            pSong.name = nameList[i];
            songs.add(pSong);
        }

        ArrayList<ShazamHash> alp = p1.getHashes(path);
        System.out.println("Finish extractiong gingerprints, start grading...");

        for(i=0; i<alp.size(); i++)
        {
            try
            {
                resultSet = dbManager.matchHash(alp.get(i));
            }
            catch (RuntimeException e)
            {
                continue;
            }

            try
            {
                while(resultSet.next())
                {
                    int id = resultSet.getInt(1);
                    int offset = resultSet.getInt(2);
                    int OFFSET = offset - alp.get(i).offset;
                    Integer times = songs.get(id - 1).hashmap.get(OFFSET);
                    songs.get(id - 1).hashmap.put(OFFSET, (times==null)?1:times++);
                }
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }//end for.It represents that the work of Match has finished.

        // start to grade.
        for(int j=0; j < SongN; j++)
        {
            HashMap hm = songs.get(j).hashmap;
            Set set = hm.keySet();
            Iterator it = set.iterator();
            int max = 0;
            while(it.hasNext())
            {
                int key = (int)it.next();
                int result = (int)hm.get(key);
                if(result > max)max = result;
            }
            songs.get(j).score = max;
        }

        //sort songs
        Collections.sort(songs);


        //print the top 5
        System.out.println("匹配的歌曲为：");
        for(int k = 0; k <5; k++)
        {
            System.out.print("[" + songs.get(k).id + "]");
            System.out.print("(" + songs.get(k).name + ")"+ "  ");
            System.out.println("score: " + songs.get(k).score);
        }
    }
}


