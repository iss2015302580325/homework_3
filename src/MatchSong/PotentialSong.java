package MatchSong;

import java.util.HashMap;

/**
 * Created by ChenCheng on 2016/11/10.
 * This class is to define a potential song with it's name,id,and final score.
 */
public class PotentialSong implements Comparable
{
    public int id;
    public String name;
    public HashMap<Integer, Integer> hashmap = new HashMap<Integer, Integer>();
    public int score;
    @Override
    public int compareTo(Object o)
    {
        PotentialSong s = (PotentialSong)o;
        return - (this.score - s.score);
    }
}
