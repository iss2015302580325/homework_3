package homework_3_by_ly;

/**
 * 
 * @author create by liuyang
 *
 */


// this class is created for figure_extract
public class ShazamHash 
{
	public int id;
	public int offset;
	public short f1;
	public short f2;
	public short dt;
		
	public ShazamHash()
	{
		id =0;
		offset =0;
		f1=0;
		f2=0;
		dt =0;
	}
	
	public void setId(int id)
	{
		this.id =id;
	}
	
	public void setOffset(int offset)
	{
		this.offset =offset;
	}
	
	public void setf1(short f1)
	{
		this.f1 =f1;
	}
	
	public void setf2(short f2)
	{
		this.f2 =f2;
	}
	
	public void setdt(short dt)
	{
		this.dt =dt;
	}

}
